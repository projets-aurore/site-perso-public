var etatDepliant1 = false;
var etatDepliant2 = false;
var etatDepliant3 = false;
var etatDepliant4 = false;
var etatDepliant5 = false;

function toggleDepliant1() {
  if(etatDepliant1 === true) {
    document.querySelector('.depliant1').classList.remove('depliant1-ouvert');
    document.querySelector('.fleche1').classList.remove('fleche1--active');
  } else {
    document.querySelector('.depliant1').classList.add('depliant1-ouvert');
    document.querySelector('.fleche1').classList.add('fleche1--active');
  }

  etatDepliant1 = !etatDepliant1;
}

function toggleDepliant2() {
    if(etatDepliant2 === true) {
      document.querySelector('.depliant2').classList.remove('depliant2-ouvert');
      document.querySelector('.fleche2').classList.remove('fleche2--active');
    } else {
      document.querySelector('.depliant2').classList.add('depliant2-ouvert');
      document.querySelector('.fleche2').classList.add('fleche2--active');
    }
  
    etatDepliant2 = !etatDepliant2;
  }

  function toggleDepliant3() {
    if(etatDepliant3 === true) {
      document.querySelector('.depliant3').classList.remove('depliant3-ouvert');
      document.querySelector('.fleche3').classList.remove('fleche3--active');
    } else {
      document.querySelector('.depliant3').classList.add('depliant3-ouvert');
      document.querySelector('.fleche3').classList.add('fleche3--active');
    }
  
    etatDepliant3 = !etatDepliant3;
  }

  function toggleDepliant4() {
    if(etatDepliant4 === true) {
      document.querySelector('.depliant4').classList.remove('depliant4-ouvert');
      document.querySelector('.fleche4').classList.remove('fleche4--active');
    } else {
      document.querySelector('.depliant4').classList.add('depliant4-ouvert');
      document.querySelector('.fleche4').classList.add('fleche4--active');
    }
  
    etatDepliant4 = !etatDepliant4;
  }

  function toggleDepliant5() {
    if(etatDepliant5 === true) {
      document.querySelector('.depliant5').classList.remove('depliant5-ouvert');
      document.querySelector('.fleche5').classList.remove('fleche5--active');
    } else {
      document.querySelector('.depliant5').classList.add('depliant5-ouvert');
      document.querySelector('.fleche5').classList.add('fleche5--active');
    }
  
    etatDepliant5 = !etatDepliant5;
  }